# serohiege

![](https://img.shields.io/badge/written%20in-Javascript-blue)

A proof-of-concept game implementing some certain mechanics.

- Drop in/out four player co-op using HTML5 Gamepad API (press Start to join; tested with XInput-compatible controllers)
- Melee and ranged attacks (use second analog stick)
- AI, enemy spawning, sprite animation, collision detection, health, dynamic viewport, death animations...
- Incorporates some assets sourced under permissive licenses, see LICENSE.txt for more detail.

Tags: game

## TODO

- Add features that were present in the inspiration game (drops, waves, level progression, persistence, menu)...
- Windows binary using nw.js

## See Also

- HeroSiege, the inspiration for the ranged attack mechanic: http://store.steampowered.com/app/269210/
- OpenGameArt: http://opengameart.org/ 
- Universal LPC Sprite sheet: https://github.com/makrohn/Universal-LPC-spritesheet 

## Changelog

2015-10-04: r0
- Initial public release
- [⬇️ serohiege-r0.7z](dist-archive/serohiege-r0.7z) *(1.95 MiB)*


2015-09-21: r0-private
- Initial private release
